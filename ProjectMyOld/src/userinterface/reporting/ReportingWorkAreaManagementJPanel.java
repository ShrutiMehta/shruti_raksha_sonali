/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.reporting;

import barchartsample.Report;
import business.customer.CarOrderItem;
import business.customer.CompanyCar;
import business.customer.CustomizedOrder;
import business.customer.CustomizedOrderItem;
import business.customer.MasterOrderList;
import business.customer.OrderAtStore;
import business.ecosystem.Ecosystem;
import business.enterprise.CustomerSupportEnterprise;
import business.enterprise.Enterprise;
import business.network.Network;
import business.organization.Organization;
import business.organization.ReportingOrganization;
import business.organization.StoreOrganization;
import business.useraccount.UserAccount;
import java.awt.CardLayout;
import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author sonalichaudhari
 *
 *
 */
public class ReportingWorkAreaManagementJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ReportingWorkAreaManagementJPanel
     */
    private MasterOrderList mod;
    private CustomizedOrder customizedOrder;
    private CustomizedOrderItem customizedOrderItem;
    private OrderAtStore orderAtStore;
    private JPanel userProcessContainer;
    private UserAccount account;
    private Enterprise enterprise;
    private Ecosystem business;
    private Organization organization;

    final String applicationTitle = "Hazardous Content Chart";
    final String chartTitle = "Hazardous Content Chart";

    public ReportingWorkAreaManagementJPanel(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, Ecosystem business) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.organization = (ReportingOrganization) organization;
        this.account = account;
        this.enterprise = enterprise;
        this.business = business;

        valueLabel.setText(enterprise.getName());
        orgValueLabel.setText(organization.getName());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cumulativeJButton = new javax.swing.JButton();
        backJButton7 = new javax.swing.JButton();
        hazardousContentjBtn = new javax.swing.JButton();
        revenueJButton5 = new javax.swing.JButton();
        enterpriseLabel2 = new javax.swing.JLabel();
        enterpriseLabel3 = new javax.swing.JLabel();
        orgValueLabel = new javax.swing.JLabel();
        valueLabel = new javax.swing.JLabel();
        organisedJButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setBackground(new java.awt.Color(179, 238, 238));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setFont(new java.awt.Font("ITF Devanagari Marathi", 1, 18)); // NOI18N
        jLabel6.setText("Management");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 70, -1, -1));

        jLabel7.setFont(new java.awt.Font("ITF Devanagari Marathi", 1, 18)); // NOI18N
        jLabel7.setText("Reporting Work Area ");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 40, -1, -1));

        cumulativeJButton.setBackground(new java.awt.Color(51, 204, 255));
        cumulativeJButton.setFont(new java.awt.Font("ITF Devanagari Marathi", 1, 18)); // NOI18N
        cumulativeJButton.setText("Cumulative ");
        cumulativeJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cumulativeJButtonActionPerformed(evt);
            }
        });
        add(cumulativeJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 350, 240, 110));

        backJButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/back.jpg"))); // NOI18N
        backJButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButton7ActionPerformed(evt);
            }
        });
        add(backJButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 80, 60));

        hazardousContentjBtn.setBackground(new java.awt.Color(51, 102, 255));
        hazardousContentjBtn.setFont(new java.awt.Font("ITF Devanagari Marathi", 1, 18)); // NOI18N
        hazardousContentjBtn.setText("Level of Hazardous Content");
        hazardousContentjBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hazardousContentjBtnActionPerformed(evt);
            }
        });
        add(hazardousContentjBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 160, 240, 110));

        revenueJButton5.setBackground(new java.awt.Color(0, 204, 255));
        revenueJButton5.setFont(new java.awt.Font("ITF Devanagari Marathi", 1, 18)); // NOI18N
        revenueJButton5.setText("Revenue ");
        revenueJButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                revenueJButton5ActionPerformed(evt);
            }
        });
        add(revenueJButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 250, 160, 130));

        enterpriseLabel2.setFont(new java.awt.Font("ITF Devanagari Marathi", 1, 14)); // NOI18N
        enterpriseLabel2.setText("Enterprise :");
        add(enterpriseLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 90, -1, -1));

        enterpriseLabel3.setFont(new java.awt.Font("ITF Devanagari Marathi", 1, 14)); // NOI18N
        enterpriseLabel3.setText("Organization:");
        add(enterpriseLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 110, -1, -1));

        orgValueLabel.setFont(new java.awt.Font("ITF Devanagari Marathi", 1, 14)); // NOI18N
        orgValueLabel.setText("<value>");
        add(orgValueLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 110, -1, -1));

        valueLabel.setFont(new java.awt.Font("ITF Devanagari Marathi", 1, 14)); // NOI18N
        valueLabel.setText("<value>");
        add(valueLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 90, -1, -1));

        organisedJButton.setBackground(new java.awt.Color(51, 204, 255));
        organisedJButton.setFont(new java.awt.Font("ITF Devanagari Marathi", 1, 18)); // NOI18N
        organisedJButton.setText("Enterprise");
        organisedJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                organisedJButtonActionPerformed(evt);
            }
        });
        add(organisedJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 240, -1, 140));

        jButton1.setText("Organisation");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 160, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void cumulativeJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cumulativeJButtonActionPerformed
        // TODO add your handling code here:
        CumulativeReportJPanel cjp = new CumulativeReportJPanel(userProcessContainer, mod,business);
        userProcessContainer.add("CumulativeReportJPanel", cjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        
        
//        Report report = new Report();
//        int lead;
//        int carbonMonoxide;
//        int formaldehyde;
//        lead = report.getCumleadContent();
//        carbonMonoxide = report.getCumCarbonMContent();
//        formaldehyde = report.getCumFMDHContent();
//
//        DefaultPieDataset pieDataset = new DefaultPieDataset();
//
//        pieDataset.setValue("Lead", new Integer(65));
//        pieDataset.setValue("Carbon Monoxide", new Integer(22));
//        pieDataset.setValue("Formaldehyde", new Integer(12));
//
//        JFreeChart chart = ChartFactory.createPieChart("Pie Chart", pieDataset, true, true, true);
//        PiePlot P = (PiePlot) chart.getPlot();
//
//        ChartFrame frame = new ChartFrame("Pie Chart", chart);
//        frame.setVisible(true);
//        frame.setSize(450, 500);
//
    }//GEN-LAST:event_cumulativeJButtonActionPerformed

    private void backJButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButton7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_backJButton7ActionPerformed

    private void hazardousContentjBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hazardousContentjBtnActionPerformed
        // TODO add your handling code here:

        LeadContentReportJPanel lcjp = new LeadContentReportJPanel(userProcessContainer, mod);
        userProcessContainer.add("LeadContentReportJPanel", lcjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);

    }//GEN-LAST:event_hazardousContentjBtnActionPerformed

    private void revenueJButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_revenueJButton5ActionPerformed
        // TODO add your handling code here: 
        RevenueReportJPanel rrjp = new RevenueReportJPanel(userProcessContainer, mod, business);
        userProcessContainer.add("LeadContentReportJPanel", rrjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);

//            JOptionPane.showMessageDialog(null, customizedRevenue, "Warning", JOptionPane.WARNING_MESSAGE);

    }//GEN-LAST:event_revenueJButton5ActionPerformed

    private void organisedJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_organisedJButtonActionPerformed

        EnterpriseBarJPanel ojp = new EnterpriseBarJPanel(userProcessContainer, mod, business);
        userProcessContainer.add("OrganizationReportJPanel", ojp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_organisedJButtonActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        OrganisationReportJPanel orjp= new OrganisationReportJPanel(userProcessContainer, mod, business);
         userProcessContainer.add("OrganizationReportJPanel", orjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backJButton7;
    private javax.swing.JButton cumulativeJButton;
    private javax.swing.JLabel enterpriseLabel2;
    private javax.swing.JLabel enterpriseLabel3;
    private javax.swing.JButton hazardousContentjBtn;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel orgValueLabel;
    private javax.swing.JButton organisedJButton;
    private javax.swing.JButton revenueJButton5;
    private javax.swing.JLabel valueLabel;
    // End of variables declaration//GEN-END:variables
}
